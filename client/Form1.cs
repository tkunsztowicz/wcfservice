﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Form1 : Form
    {
        int[][] matrix1;
        int[][] matrix2;
        int[][] resultmatrix;
        int matrix1ID;
        int matrix2ID;
        int resultID;
        ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //send matrix 2
            matrix2ID = client.SendMatrix(matrix2);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();

            MatricesReadWrite obMatrix1 = new MatricesReadWrite();
            obMatrix1.ReadMatrixFromStreamreader(openFileDialog1.FileName);
            matrix1 = obMatrix1.Matrix;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();

            MatricesReadWrite obMatrix2 = new MatricesReadWrite();
            obMatrix2.ReadMatrixFromStreamreader(openFileDialog2.FileName);
            matrix2 = obMatrix2.Matrix;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //send matrix 1
            matrix1ID = client.SendMatrix(matrix1);
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //calculate
            resultID = client.Multiply(matrix1ID, matrix2ID);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //get result
            resultmatrix = client.GetMatrix(resultID);
            MatricesReadWrite obMatrix3 = new MatricesReadWrite();
            obMatrix3.WriteMatrixtFile(resultmatrix, @"C:\matrixresult.txt");
            MessageBox.Show("done");
        }
    }
}
