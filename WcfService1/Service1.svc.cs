﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Service1 : IService1
    {
        int k = 0;
        ArrayList array = new ArrayList();
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public int SendMatrix(int[][] value)
        {
            array.Add(value);
       
            
            return array.IndexOf(value);
        }
        public int Multiply(int id1, int id2)
        {
            MatrixOperations operation = new MatrixOperations();
            int[][] resultMatrix = operation.MatrixMultiply((int[][])array[id1], (int[][])array[id2]);
            array.Add(resultMatrix);
            return array.IndexOf(resultMatrix);
        }

        public int[][] GetMatrix(int id1)
        {
            return (int[][])array[id1];
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public string Test()
        {
            return "Hello World";
            
        }
        public string Test2()
        {
            return "Hello World";

        }
    }
}
