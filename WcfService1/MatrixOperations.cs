﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace WcfService1
{
    public class MatrixOperations
    {
        long calcTime;

        public long CalcTime
        {
            get
            {
                return calcTime;
            }
        }
       
        public int[][] MatrixMultiply(int[][] matA, int[][] matB)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            
            int matACols = matA[0].GetLength(0);
            int matBCols = matB[0].GetLength(0);
            int matARows = matA.GetLength(0);
            int[][] result = new int[matBCols][];
            for (int i = 0; i < matBCols; i++)
            {
                result[i] = new int[matARows];
            }
            // A basic matrix multiplication.
            // Parallelize the outer loop to partition the source array by rows.
            Parallel.For(0, matARows, i =>
            {
                for (int j = 0; j < matBCols; j++)
                {
                    int temp = 0;
                    for (int k = 0; k < matACols; k++)
                    {
                        temp += matA[i][k] * matB[k][j];
                    }
                    result[i][j] = temp;
                }
            }); // Parallel.For
            stopwatch.Stop();
            calcTime = stopwatch.ElapsedMilliseconds;
            return result;
        
        }
    }
}